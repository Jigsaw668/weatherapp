import React, { useState, useEffect } from 'react';

import City from './components/City';
import CheckCity from './components/CheckCity';
import TemperatureUI from './components/TemperatureUI';
import WeatherDisplay from './components/WeatherDisplay';

import { API_KEY } from './configuration/config';
import { CITY } from './configuration/city.list';

import './App.scss';
import CheckScale from './components/CheckScale/CheckScale';

const getCityId = (name) => {
  return CITY.filter(city => city.name === name)[0].id;
}

const App = () => {
  const [data, setData] = useState({});
  const [cityName, setCityName] = useState('Омск'); // there should be a city taken from geodata
  const [tempScale, setTempScale] = useState('celsius');

  const updateCityName = (name) => {
    setCityName(name);
  };

  const updateTempScale = (scale) => {
    setTempScale(scale);
  };

  useEffect(() => {
    fetch(`http://api.openweathermap.org/data/2.5/weather?id=${getCityId(cityName)}&lang=ru&units=metric&APPID=${API_KEY}`)
      .then(response => response.json())
      .then(data => setData({
        temp: data.main.temp,
        pressure: data.main.pressure,
        humidity: data.main.humidity,
        weather: data.weather[0].description,
        weatherIcon: data.weather[0].icon,
        clouds: data.clouds.all,
        windSpeed: data.wind.speed,
        windDeg: 260
      }));
  }, [cityName]);

  return (
    <div className='App'>
      <div className='header'>
        <City name={ cityName } />
        <CheckScale scale={tempScale} checkScale={updateTempScale} />
      </div>
      <CheckCity check={updateCityName} />
      <TemperatureUI temp={data.temp} description={data.weather} icon={data.weatherIcon} scale={tempScale} />
      <WeatherDisplay data={ data } />
    </div>
  );
}

export default App;
