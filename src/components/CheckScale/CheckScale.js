import React from 'react';

import './CheckScale.scss';

const CheckScale = ({ scale, checkScale }) => {
  return (
    <div className='scale'>
      <label htmlFor='scale-celsius' className={scale === 'celsius' ? 'scale__label active' : 'scale__label'}>C
        <input id='scale-celsius' type='radio' name='scale' value='celsius' onClick={e => checkScale(e.target.value)} defaultChecked />
      </label>
      <label htmlFor='scale-fahrenheit' className={scale === 'fahrenheit' ? 'scale__label active' : 'scale__label'}>F
        <input id='scale-fahrenheit' type='radio' name='scale' value='fahrenheit' onClick={e => checkScale(e.target.value)} />
      </label>
    </div>
  );
};

export default CheckScale;
 