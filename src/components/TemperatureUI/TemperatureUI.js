import React from 'react';

import './TemperatureUI.scss';

const TemperatureUI = ({ temp, description, icon, scale }) => {
  const convertTemp = (t, scale) => {
    switch(scale) {
      case 'fahrenheit':
        return ((t * (9 / 5)) + 32).toFixed(1);
      default:
        return t.toFixed(1);
    }
  };

  return (
    <div className='temp'>
      <div className='temp__icon'>
        { icon &&
          <img src={`http://openweathermap.org/img/wn/${icon}@2x.png`} alt='weather-icon' />
        }
      </div>
      <div className='temp__value'>
        { temp &&
          convertTemp(temp, scale)
        }
      </div>
      <div className='temp__description'>
        {description}
      </div>
    </div>
  )
}

export default TemperatureUI;
