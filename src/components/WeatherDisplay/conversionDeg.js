const conversionWindDeg = (deg) => {
  if (deg) {
    if (deg === 360 || deg === 0) {
      return 'северный'
    } else if (0 < deg && deg < 45) {
      console.log('1)')
      return 'северо-северо-восточный';
    } else if (deg === 45) {
      return 'северо-восточный';
    } else if (45 < deg && deg < 90) {
      return 'восточно-северо-восточный';
    } else if (deg === 90) {
      return 'восточный';
    } else if (90 < deg && deg < 135) {
      return 'восточно-юго-восточный';
    } else if (deg === 135) {
      return 'юго-восточный';
    } else if (135 < deg && deg < 180) {
      return 'юго-юго-восточный';
    } else if (deg === 180) {
      return 'южный';
    } else if (180 < deg && deg < 225) {
      return 'юго-юго-западный';
    } else if (deg === 225) {
      return 'юго-западный';
    } else if (225 < deg && deg < 270) {
      return 'западно-юго-западный';
    } else if (deg === 270) {
      return 'западный';
    } else if (270 < deg && deg < 305) {
      return 'западно-северо-западный';
    } else if (deg === 305) {
      return 'северо-западный';
    } else if (305 < deg && deg < 360) {
      return 'северо-северо-западный';
    }
  }
};

export default conversionWindDeg;
