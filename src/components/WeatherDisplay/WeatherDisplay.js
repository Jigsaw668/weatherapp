import React from 'react';

import WeatherDescription from '../WeatherDescription';
import conversionDeg from './conversionDeg';

import './WeatherDisplay.scss';

const WeatherDisplay = ({ data }) => {
  const wind = `${data.windSpeed || '--'} м/с, ${conversionDeg(data.windDeg)}`;
  const pressure = `${data.pressure * 0.75 || '--'} мм рт. ст.`;
  const humidity = `${data.humidity || '--'} %`;
  const clouds = `${data.clouds || '--'} %`;

  return (
    <div className='weather'>
      <WeatherDescription title={'Ветер'} value={ wind } />
      <WeatherDescription title={'Давление'} value={ pressure } />
      <WeatherDescription title={'Влажность'} value={ humidity } />
      <WeatherDescription title={'Облачность'} value={ clouds } />
    </div>
  )
}

export default WeatherDisplay;
