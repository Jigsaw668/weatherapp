import React from 'react';

import './WeatherDescription.scss';

const WeatherDescription = ({title, value}) => {
  return (
    <div className='weather-wrap'>
      <h3 className='weather__title'>{title}</h3>
      <p className='weather__description'>{value}</p>
    </div>
  );
};

export default WeatherDescription;
