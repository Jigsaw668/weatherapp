import React from 'react';

import './City.scss';

const City = ({ name }) => {
  return (
    <div className='city'>
      <h1 className='city__title'>{name}</h1>
    </div>
  );
};

export default City;
