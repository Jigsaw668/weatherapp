import React, { useState } from 'react';
import Popup from "reactjs-popup";

import { CITY } from '../../configuration/city.list';

import './CheckCity.scss';

const CheckCity = (updateCityName) => {
  const [open, setOpen] = useState(false);

  return (
    <div className='check-city'>
      <button className='check-city__button' onClick={() => setOpen(true)}>
        Выберите город
      </button>
      <Popup open={open} closeOnDocumentClick onClose={() => setOpen(false)} position="bottom center">
        {
          CITY.map((city, i) => {
            return (
              <button key={i} onClick={e => {
                updateCityName.check(e.currentTarget.innerText);
                setOpen(false);
              }}>{city.name}</button>
            );
          })
        }
      </Popup>
      <button className='check-city__button' onClick={e => updateCityName.check('Омск')}>Мое местоположение</button>
    </div>
  );
};

export default CheckCity;
