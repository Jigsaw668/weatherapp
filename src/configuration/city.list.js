export const CITY = [
  {
    "id": 1496153,
    "name": "Омск",
    "country": "RU",
    "coord": {
      "lon": 73.400002,
      "lat": 55
    }
  },
  {
    "id": 1496747,
    "name": "Новосибирск",
    "country": "RU",
    "coord": {
      "lon": 82.934441,
      "lat": 55.041111
    }
  },
  {
    "id": 524894,
    "name": "Москва",
    "country": "RU",
    "coord": {
      "lon": 37.606667,
      "lat": 55.761665
    }
  }
]